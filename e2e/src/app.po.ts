import {browser, by, element} from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getZonesCalculator() {
    return element(by.tagName('app-root app-zones-calculator'));
  }
}
