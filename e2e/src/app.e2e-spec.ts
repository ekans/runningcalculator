import {AppPage} from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display the zones calculator', () => {
    page.navigateTo();
    expect(page.getZonesCalculator()).toBeDefined();
  });
});
