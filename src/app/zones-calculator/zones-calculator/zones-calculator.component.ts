import {Component} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-zones-calculator',
  templateUrl: './zones-calculator.component.html',
  styleUrls: ['./zones-calculator.component.css']
})
export class ZonesCalculatorComponent {

  hrMax = new FormControl('', [
    Validators.required,
    Validators.min(150),
    Validators.max(230)
  ]);

  hrRest = new FormControl('', [
    Validators.required,
    Validators.min(20),
    Validators.max(100)
  ]);
}
