import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ZonesCalculatorComponent} from './zones-calculator.component';
import {ZoneComponent} from '../zone/zone.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('ZonesCalculatorComponent', () => {
  let component: ZonesCalculatorComponent;
  let fixture: ComponentFixture<ZonesCalculatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ZonesCalculatorComponent, ZoneComponent],
      imports: [NoopAnimationsModule, FormsModule, ReactiveFormsModule, MatInputModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZonesCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
