import { ZonesCalculatorModule } from './zones-calculator.module';

describe('ZonesCalculatorModule', () => {
  let zonesCalculatorModule: ZonesCalculatorModule;

  beforeEach(() => {
    zonesCalculatorModule = new ZonesCalculatorModule();
  });

  it('should create an instance', () => {
    expect(zonesCalculatorModule).toBeTruthy();
  });
});
