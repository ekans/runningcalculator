import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-zone',
  templateUrl: './zone.component.html',
  styleUrls: ['./zone.component.css']
})
export class ZoneComponent implements OnChanges {

  @Input() hrMax: number | null = null;
  @Input() hrRest: number | null = null;
  @Input() maxPercentage: number | null = null;

  value = 0;

  ngOnChanges(changes: SimpleChanges): void {
    if (this.hrMax == null || this.hrRest == null || this.maxPercentage == null) {
      return;
    }

    this.value = this.compute(this.hrMax, this.hrRest, this.maxPercentage);
  }

  private compute(hrMax: number, hrRest: number, percentage: number): number {
    const result = ((hrMax - hrRest) * percentage + hrRest * 100) / 100.00;
    return Math.round(result);
  }
}
