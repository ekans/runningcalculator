import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ZonesCalculatorComponent} from './zones-calculator/zones-calculator.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material';
import {ZoneComponent} from './zone/zone.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
  ],
  declarations: [ZonesCalculatorComponent, ZoneComponent],
  exports: [ZonesCalculatorComponent]
})
export class ZonesCalculatorModule {
}
