import {async, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {ZonesCalculatorModule} from './zones-calculator/zones-calculator.module';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [ZonesCalculatorModule]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
